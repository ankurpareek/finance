Steps to run this project
Install go before proceeding
Steps:
1. cd /$GOPATH/src
2. git clone {repository_url}
3. go get -u github.com/kataras/iris
4. go get -u github.com/go-sql-driver/mysql
