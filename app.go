package main

import (
	"finance/sms"
	"github.com/kataras/iris"
)

func main() {

	//Initialize app
	app := iris.New()

	app.Handle("GET", "/updateSmsStatus", func(ctx iris.Context) {
		params := ctx.FormValues()
		response := sms.UpdateStatus(params)
		ctx.JSON(response)
	})

	app.Run(iris.Addr("localhost:5000"))

}
