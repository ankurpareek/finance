package database

import (
	"database/sql"
	"finance/config"
)

func GetMySQLDB() (db *sql.DB, err error) {
	db, err = sql.Open(config.DbDriver, config.DbUser+":"+config.DbPass+"@"+config.DbHost+"/"+config.DbName)
	return
}
