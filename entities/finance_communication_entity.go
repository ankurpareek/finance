package entities

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
)

type FinanceCommunication struct {
	Id            int64
	Phone         int64
	Mid           int64
	Reason        sql.NullString
	Delivery_date mysql.NullTime
	Status        int64
}
