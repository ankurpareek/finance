package models

import (
	"database/sql"
	"finance/entities"
	"github.com/go-sql-driver/mysql"
)

type FinanceCommunicationModel struct {
	Db *sql.DB
}

func (financeComm FinanceCommunicationModel) FindByMid(mid string) (entities.FinanceCommunication, error) {
	message := entities.FinanceCommunication{}
	rows, err := financeComm.Db.Query("SELECT id, phone, mid, reason, delivery_date, status FROM finance_communication_sms_log where mid = ? ", mid)
	defer rows.Close()

	if err != nil {
		return entities.FinanceCommunication{}, err
	} else {
		for rows.Next() {
			var id int64
			var phone int64
			var mid int64
			var reason sql.NullString
			var delivery_date mysql.NullTime
			var status int64
			rows.Scan(&id, &phone, &mid, &reason, &delivery_date, &status)
			message = entities.FinanceCommunication{id, phone, mid, reason, delivery_date, status}
			return message, nil
		}
	}
	return message, nil
}
