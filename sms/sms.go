package sms

import (
	"finance/database"
	"finance/models"
	"time"
)

func UpdateStatus(params map[string][]string) map[string]interface{} {
	response := map[string]interface{}{"status": false, "message": ""}

	db, err := database.GetMySQLDB()
	defer db.Close()
	if err != nil {
		return response
	} else {
		financeCommModel := models.FinanceCommunicationModel{
			Db: db,
		}

		message, _ := financeCommModel.FindByMid(params["request_id"][0])

		if message.Id == 0 {
			response["message"] = "No result found"
		} else {
			layout := "2006-01-02 15:04:05"
			str := params["completed_at"][0]
			delivery_date, err := time.Parse(layout, str)

			if err != nil {
				response["message"] = "Invalid date"
				return response
			}
			_, error := db.Exec("Update finance_communication_sms_log SET reason=?, delivery_date=?, status=? where mid = ?", params["reason"][0], delivery_date, params["status"][0], params["request_id"][0])
			if error != nil {
				response["message"] = "Some Error Occured"
			} else {
				response["status"] = true
				response["message"] = "Successfully updated the status"
			}
		}

	}

	return response
}
